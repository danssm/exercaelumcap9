
public class TestaContas {
  public static void main(String[] args) {
    Conta c = new ContaCorrente();
    // Mesmo sem o "new Conta()" é vantajoso ter um método que importe Conta, 
    //pois isso permite que tanto ContaCorrente quanto ContaPoupanca possam 
    //entrar como método. Isso se chama polimorfismo.
    ContaCorrente cc = new ContaCorrente();
    ContaPoupanca cp = new ContaPoupanca();
  
    c.deposita(1000); 
    cc.deposita(1000); 
    cp.deposita(1000);
  
    c.atualiza(0.01);
    cc.atualiza(0.01);
    cp.atualiza(0.01);

    System.out.println(c.getSaldo());
    System.out.println(cc.getSaldo());
    System.out.println(cp.getSaldo());

  }
}