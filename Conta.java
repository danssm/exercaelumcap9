
public abstract class Conta {
	// Ao fazer isso a classe TestaContas apresentou erro, pois não se pode instânciar uma classe abstract
	protected double saldo;
	public void deposita(double valor) {
		this.saldo += valor;
	}

	public void saca(double valor) {
	    this.saldo -= valor;
	}

	public double getSaldo() {
	    return this.saldo;
	}
	
	abstract public void atualiza(double taxa);
}
